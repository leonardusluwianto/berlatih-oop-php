<?php

require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

// RELEASE 0
$sheep = new Animal("shaun");

echo "Name of animal: $sheep->name <br>"; // "shaun"
echo "Number of legs: $sheep->legs <br>"; // 2
echo "Cold-blooded: $sheep->cold_blooded <br><br>"; // false

// RELEASE 1
$sungokong = new Ape("kera sakti");
echo "Name of animal: $sungokong->name <br>"; // "kera sakti"
echo "Number of legs: $sungokong->legs <br>"; // 2
echo "Cold-blooded: $sungokong->cold_blooded <br>"; // false
echo $sungokong->yell(); // "Auooo"
echo "<br><br>";

$kodok = new Frog("buduk");
echo "Name of animal: $kodok->name <br>"; // "buduk"
echo "Number of legs: $kodok->legs <br>"; // 4
echo "Cold-blooded: $kodok->cold_blooded <br>"; // true
echo $kodok->jump(); // "hop hop"

?>
